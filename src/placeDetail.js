function placeDetail (position){
	console.log("Hello");
	var defered = $.Deferred();
	var geocoder = new google.maps.Geocoder();
	var latlng = new google.maps.LatLng(position.lat, position.lng);
	geocoder.geocode(
	    {'latLng': latlng}, 
	    function(results, status) {
	        if (status == google.maps.GeocoderStatus.OK) {
	        	console.log(results)
				if (results[0]) {
					var res = '<div class="infoPopup"><p>' + results[0].formatted_address + '</p></div>';
					defered.resolve(res);
				}else  {
					defered.resolve("<div><p>No Address Found.</p></div>")
				}
			}
	         else {
	            alert("Geocoder failed due to: " + status);
	        }
	    }
	);
	return defered.promise();
}