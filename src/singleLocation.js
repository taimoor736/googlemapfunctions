var locations = [
		['Abtabad', 34.1558, 73.2194, 4],
		['Karachi', 24.8934, 67.0281, 5],
		['ISL', 33.7182, 73.0605, 3],
    ];
var currentPosition;
function initMap (){
	var map = new google.maps.Map(document.getElementById('map'), {
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		center: {lat: -34.397, lng: 150.644},
		zoom: 15
	});
        // Try HTML5 geolocation.
    if (navigator.geolocation) {
    	console.log(navigator.geolocation);
        navigator.geolocation.getCurrentPosition(function(position) {
        console.log(position);
        currentPosition = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
        };
        var marker = new google.maps.Marker({
            map: map,
            position: currentPosition,
            title : "Your Location"
        });
        map.setCenter(currentPosition);
		marker.addListener('click', function() {
			placeDetail(currentPosition)
            .then(function(res){
    			var infoWindow = new google.maps.InfoWindow({map: map,content : res});
    			infoWindow.open(map, marker);
            })
		});
        }, function() {
            console.log("here");
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        handleLocationError(false, infoWindow, map.getCenter());
    }
}
function handleLocationError(browserHasGeolocation, infoWindow, currentPosition) {
	infoWindow.setPosition(currentPosition);
	infoWindow.setContent(browserHasGeolocation ?
	'Error: The Geolocation service failed.' :
	'Error: Your browser doesn\'t support geolocation.');
}
function getLocation(){
    getAttraction(locations);
}
function directions(index){
	var destination = {"name " : locations[index][0],"lat" : locations[index][1], "lng" : locations[index][2]}
	getDirections(currentPosition,destination);
}