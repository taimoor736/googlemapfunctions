function getDirections(origin,destination){
	var originCord = new google.maps.LatLng(origin.lat,origin.lng); 
	var destCords = new google.maps.LatLng(destination.lat,destination.lng);
	console.log(destination);
	var directionsService = new google.maps.DirectionsService();
	var directionsDisplay = new google.maps.DirectionsRenderer();

	var mapOpt = {
		zoom : 6,
		center : originCord,
		mapTypeControl: true,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	var map = new google.maps.Map(document.getElementById("map"), mapOpt);
	directionsDisplay.setMap(map);
	var marker = new google.maps.Marker({
		map: map,
		position: originCord,
		title : "Start"
    });
    var marker2 = new google.maps.Marker({
		map: map,
		position: destCords,
		title : "End"
    });
	var request = {
		origin : originCord,
		destination : destCords,
		travelMode: google.maps.DirectionsTravelMode.DRIVING
	}
	directionsService.route(request, function (response, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			directionsDisplay.setDirections(response);
		}
	});
}